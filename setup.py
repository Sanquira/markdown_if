from setuptools import setup

setup(
    name='markdown_if',
    version='0.3',
    description='Markdown extension to satisfy Immortal Fighters standards.',
    author='Sanquira Van Delbar',
    author_email='sanquira.van.delbar@gmail.com',
    packages=['markdown_if'],
    url='https://bitbucket.org/Sanquira/markdown_if',
    install_requires=['markdown>=2.5'],
)
