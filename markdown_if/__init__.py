from typing import Dict, List

from markdown.extensions import Extension
from markdown.extensions.tables import TableProcessor
from markdown.inlinepatterns import SimpleTagPattern, Pattern
from markdown.util import etree


class MyTableProcessor(TableProcessor):
    
    def run(self, parent, blocks):
        """ Parse a table block and build table. """
        block = blocks.pop(0).split('\n')
        header = block[0].strip(' ')
        rows = [] if len(block) < 3 else block[2:]
        
        # Get alignment of columns
        align = []
        for c in self.separator:
            c = c.strip(' ')
            if c.startswith(':') and c.endswith(':'):
                align.append('center')
            elif c.startswith(':'):
                align.append('left')
            elif c.endswith(':'):
                align.append('right')
            else:
                align.append(None)
        
        # Build table
        table = etree.SubElement(parent, 'table')
        table.set('class', 'markdown-table')
        thead = etree.SubElement(table, 'thead')
        self._build_row(header, thead, align)
        tbody = etree.SubElement(table, 'tbody')
        if len(rows) == 0:
            # Handle empty table
            self._build_empty_row(tbody, align)
        else:
            for row in rows:
                self._build_row(row.strip(' '), tbody, align)


class StyledPattern(SimpleTagPattern):
    """ Define pattern with custom styles.
    
        Add <tag style="name1:value1;name2:value2;etc.">content</tag>
        """
    
    def __init__(self, *args, **kwargs):
        super(StyledPattern, self).__init__(*args, **kwargs)
        self._style_set = {}
    
    def addStyles(self, styles: Dict[str, str]):
        self._style_set.update(styles)
    
    def addStyle(self, style: str, value: str) -> bool:
        if style in self._style_set:
            return False
        self._style_set[style] = value
        return True
    
    def removeStyle(self, style: str) -> bool:
        if style in self._style_set:
            del self._style_set[style]
            return True
        return False
    
    def handleMatch(self, m):
        el = super(StyledPattern, self).handleMatch(m)
        if self._style_set:
            stylestr = ""
            for style, value in self._style_set.items():
                stylestr += "%s:%s;" % (style, value)
            el.set("style", stylestr)
        return el


class ClassedPattern(SimpleTagPattern):
    """ Define pattern with custom classes.
        
        Add <tag class="class1 class2 etc.">content</tag>
        """
    
    def __init__(self, *args, **kwargs):
        super(ClassedPattern, self).__init__(*args, **kwargs)
        self._class_list = []
    
    def addClasses(self, clss: List[str]):
        self._class_list.extend(clss)
    
    def addClass(self, cls: str) -> bool:
        if cls in self._class_list:
            return False
        self._class_list.append(cls)
        return True
    
    def removeClass(self, cls: str) -> bool:
        if cls in self._class_list:
            self._class_list.remove(cls)
            return True
        return False
    
    def handleMatch(self, m):
        el = super(ClassedPattern, self).handleMatch(m)
        if self._class_list:
            clsstr = ""
            for value in self._class_list:
                clsstr += "%s " % value
            el.set("class", clsstr.strip())
        return el


class ClassedStyledPattern(ClassedPattern, StyledPattern):
    """ Define pattern with custom classes and styles.
    
        Add <tag class="class1 class2 etc." style="name1:value1;name2:value2;etc.">content</tag>
        """
    
    def handleMatch(self, m):
        el = super(ClassedStyledPattern, self).handleMatch(m)
        if self._class_list:
            clsstr = ""
            for value in self._class_list:
                clsstr += "%s " % value
            el.set("class", clsstr.strip())
        if self._style_set:
            stylestr = ""
            for style, value in self._style_set.items():
                stylestr += "%s:%s;" % (style, value)
            el.set("style", stylestr)
        return el


class IFExtension(Extension):
    MULTI_RE = r'(\[%(0)s\]|\[%(1)s\])(.*?)(\[/%(0)s\]|\[/%(1)s\])'  # case insensitive
    
    def extendMarkdown(self, md):
        if '|' not in md.ESCAPED_CHARS:
            md.ESCAPED_CHARS.append('|')
        md.parser.blockprocessors.register(MyTableProcessor(md.parser), 'table', 75)
        
        del md.inlinePatterns['backtick']
        
        md.inlinePatterns.register(self.createSinglePattern("B", "strong"), "if_strong", 200)  # [B][/B]
        md.inlinePatterns.register(self.createSinglePattern("I", "em"), "if_emphasis", 210)  # [I][/I]
        md.inlinePatterns.register(self.createStylePattern("U", "u", {"text-decoration": "underline"}), "underline", 220)  # [U][/U]
        md.inlinePatterns.register(self.createClassPattern("OR", "span", ["markdown-language-human"]), "language_human", 230)  # [OR][/OR]
        md.inlinePatterns.register(self.createClassPattern("EL", "span", ["markdown-language-elf"]), "language_elf", 240)  # [EL][/EL]
        md.inlinePatterns.register(self.createClassPattern("BA", "span", ["markdown-language-barbarian"]), "language_barbarian", 250)  # [BA][/BA]
        md.inlinePatterns.register(self.createClassPattern("TR", "span", ["markdown-language-dwarf"]), "language_dwarf", 260)  # [TR][/TR]
        md.inlinePatterns.register(self.createClassPattern("OJ", "span", ["markdown-language-others"]), "language_others", 270)  # [OJ][/OJ]
        md.inlinePatterns.register(self.createClassPattern("CG", "span", ["markdown-language-green"]), "language_green", 280)  # [CG][/CG]
        md.inlinePatterns.register(self.createClassPattern("CY", "span", ["markdown-language-yellow"]), "language_yellow", 290)  # [CY][/CY]
        md.inlinePatterns.register(self.createClassPattern("CR", "span", ["markdown-language-red"]), "language_red", 300)  # [CR][/CR]
    
    def createSinglePattern(self, pattern: str, tag: str) -> SimpleTagPattern:
        return SimpleTagPattern(self.MULTI_RE % {'0': pattern, '1': pattern.lower()}, tag)
    
    def createClassPattern(self, pattern: str, tag: str, classset: List[str]) -> ClassedPattern:
        tmp = ClassedPattern(self.MULTI_RE % {'0': pattern, '1': pattern.lower()}, tag)
        tmp.addClasses(classset)
        return tmp
    
    def createStylePattern(self, pattern: str, tag: str, styles: Dict[str, str]) -> StyledPattern:
        tmp = StyledPattern(self.MULTI_RE % {'0': pattern, '1': pattern.lower()}, tag)
        tmp.addStyles(styles)
        return tmp
    
    def createClassStylePattern(self, pattern: str, tag: str, classset: List[str], styles: Dict[str, str]) -> ClassedStyledPattern:
        tmp = ClassedStyledPattern(self.MULTI_RE % {'0': pattern, '1': pattern.lower()}, tag)
        tmp.addStyles(styles)
        tmp.addClasses(classset)
        return tmp
        
def makeExtension(**kwargs):
    return IFExtension(**kwargs)
